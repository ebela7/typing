import { Link, FormattedMessage, useIntl } from 'gatsby-plugin-intl';
import React from 'react';

import { Layout, SEO, ResetInfoBox } from '@components';
import { ROUTE_PATH_CONTRIBUTION, ROUTE_PATH_CONTACT } from '@routes';

const FaqPage = () => {
  const intl = useIntl();
  const lang = intl.locale;

  function HungarianFAQ() {
    return (
      <dl>
        <dt>Mi lett az előző programmal?</dt>
        <dd>
          A 2021 előtt elérhető prorgam technikai okok miatt nem volt
          fenntartható. Egy ideig még a{' '}
          <a href="http://flash.manonet.org" target="_blank">
            flash.manonet.org/
          </a>{' '}
          cím alatt még eléerhető lesz.
        </dd>
        <dt>Miért nem működik rendesen a program?</dt>
        <dd>
          Ez a program fejlesztés alatt áll, folyamatosan próbálom javítani.
          Kérlek, légy türelemmel. Addig is, lehet{' '}
          <Link to={ROUTE_PATH_CONTRIBUTION}>támogatni</Link> vagy{' '}
          <Link to={ROUTE_PATH_CONTACT}>kérdezni</Link>.
        </dd>
        <dt>
          Miért nem jegyzi meg a leckéket? Miért kell mindig előröl kezdeni a
          gyakorlást?
        </dt>
        <dd>
          A program jelenleg az adott számítógépen rögzíti a haladást, még nem a
          fiók alatt. A bejelentkezés még nem menti szerverre az adatokat. Ez
          azt jelenti, hogy a böngészési előzmények törlése törli a haladást, ha
          az a helyben tárolt tartalomra is kiterjed. Ugyanez vonatkozik
          inkognitó-ablakra is.
        </dd>

        <dt>
          Why there is no translation for my language, or why some of the
          strings are in English?
        </dt>
        <dd>
          Unfortunately, I can not translate the page for every language. My
          dear friends were kind enough to help me out, and translate some.
          Please help, and{' '}
          <Link to={ROUTE_PATH_CONTACT}>report the issues</Link>, or help by the{' '}
          <a
            href="https://gitlab.com/zyxneo/typing/-/blob/dev/CONTRIBUTING.md#translations"
            target="_blank"
          >
            translation
          </a>{' '}
          on your own.
        </dd>
        <dt>Azt hiszem, találtam egy hibát... Mit tegyek?</dt>
        <dd>
          Az könnyen lehet, kérlek <Link to={ROUTE_PATH_CONTACT}>jelezd</Link>,
          hogy kijavíthassam!
        </dd>
        <dt>Miért nem szerepel itt a kérdésem?</dt>
        <dd>
          Mert még nem kérdezték elegen. Kérlek{' '}
          <Link to={ROUTE_PATH_CONTACT}>kérdezz</Link>!
        </dd>
      </dl>
    );
  }

  return (
    <Layout className="faq">
      <SEO
        title={intl.formatMessage({
          id: 'faq.page.title',
          defaultMessage: 'Frequently Asked Questions',
        })}
      />
      <section className="faq__section">
        <div className="container">
          <h2 className="faq__title">
            <FormattedMessage
              id="faq.page.title"
              defaultMessage="Frequently Asked Questions"
            />
          </h2>

          {intl.locale === 'hu' && <HungarianFAQ />}

          <ResetInfoBox />
        </div>
      </section>
    </Layout>
  );
};

export default FaqPage;
