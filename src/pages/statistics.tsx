import classNames from 'classnames';
import { FormattedMessage, useIntl } from 'gatsby-plugin-intl';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { Layout, SEO, Button } from '@components';
import { State as ReduxState } from '@reducers';
import './statistics.scss';

export default function StatisticsPage() {
  const intl = useIntl();
  const [showDebug, setShowDebug] = useState(false);
  const { locale } = intl;

  const {
    allChars,
    charComplianceRatio,
    charToLearn,
    charsIntroduced,
    charsToLearn,
    complianceRatio,
    cursorAt,
    displayedLevel,
    explorerMode,
    finishedLessonPractices,
    finishedPractices,
    inputChanged,
    isCapsLockOn,
    isCharIntroduced,
    isDiscoveryNeeded,
    isPracticeAccomplished,
    isPracticeFinished,
    isPracticing,
    keyToLearn,
    keysDown,
    layout,
    lessonText,
    levelToLearn,
    levels,
    name,
    os,
    practiceLength,
    practiceScrollIndex,
    practiceStatistics,
    practiceTextLetterArray,
    signToWrite,
    userText,
    writtenSign,
  } = useSelector((state: ReduxState) => state.typing);

  allChars.sort((a, b) =>
    a.glyph.localeCompare(b.glyph, locale, {
      sensitivity: 'base',
      numeric: true,
    })
  );

  function renderLessonStatisticItem(char: string, index: number) {
    const charStats = allChars.find((item) => item.glyph === char);
    const correctTotalCharHits = (charStats && charStats.correct) || 0;
    const misspellTotalCharHits = (charStats && charStats.misspell) || 0;
    const misreadTotalCharHits = (charStats && charStats.misread) || 0;
    const incorrectTotalCharHits = misspellTotalCharHits + misreadTotalCharHits;

    return (
      <div className="lessonStat" key={char}>
        <span className="lessonStat__glyph">{char}</span>
        <div className="lessonStat__details">
          <div className="lessonStat__title">
            <FormattedMessage
              id="lesson.title"
              defaultMessage="Lesson {numberOfLesson}"
              values={{ numberOfLesson: index + 1 }}
            />
          </div>
          <span className="lessonStat__all">
            <FormattedMessage id="statistics.hits.all" defaultMessage="All" />:{' '}
            {correctTotalCharHits + misspellTotalCharHits}
          </span>
          <span className="lessonStat__correct">
            <FormattedMessage
              id="statistics.hits.correct"
              defaultMessage="Correct hits"
            />
            : {correctTotalCharHits}
          </span>
          <span className="lessonStat__misspell">
            <FormattedMessage
              id="statistics.hits.misspelt"
              defaultMessage="Misspelt"
            />
            : {misspellTotalCharHits}
          </span>
          <span className="lessonStat__misread">
            <FormattedMessage
              id="statistics.hits.misread"
              defaultMessage="Misread"
            />
            : {incorrectTotalCharHits}
          </span>
        </div>
      </div>
    );
  }

  function renderBoolean(boolean: boolean) {
    return boolean ? (
      <span className="true">true</span>
    ) : (
      <span className="false">false</span>
    );
  }
  function renderStateProps() {
    return (
      <>
        <Button onClick={() => setShowDebug(!showDebug)}>Debug info</Button>
        <div
          className={classNames('stateProps', {
            'stateProps--isOpen': showDebug,
          })}
        >
          <div>
            allChars:{' '}
            {allChars.map((c) => (
              <span key={c.glyph}>{c.glyph} </span>
            ))}
          </div>
          <div>charComplianceRatio: {charComplianceRatio}</div>
          <div>charToLearn: {charToLearn}</div>
          <div>charsToLearn: {charsToLearn}</div>
          <div>complianceRatio: {complianceRatio}</div>
          <div>cursorAt: {cursorAt}</div>
          <div>displayedLevel: {displayedLevel}</div>
          <div>finishedLessonPractices: {finishedLessonPractices}</div>
          <div>finishedPractices: {finishedPractices}</div>
          <div>explorerMode: {renderBoolean(explorerMode)}</div>
          <div>inputChanged: {renderBoolean(inputChanged)}</div>
          <div>isCapsLockOn: {renderBoolean(isCapsLockOn)}</div>
          <div>isCharIntroduced: {renderBoolean(isCharIntroduced)}</div>
          <div>isDiscoveryNeeded: {renderBoolean(isDiscoveryNeeded)}</div>
          <div>
            isPracticeAccomplished: {renderBoolean(isPracticeAccomplished)}
          </div>
          <div>isPracticeFinished: {renderBoolean(isPracticeFinished)}</div>
          <div>isPracticing: {renderBoolean(isPracticing)}</div>
          <div>keyToLearn: {keyToLearn}</div>
          <div>layout: {layout}</div>
          <div>lessonText: {lessonText}</div>
          <div>levelToLearn: {levelToLearn}</div>
          <div>name: {name}</div>
          <div>practiceLength: {practiceLength}</div>
          <div>practiceScrollIndex: {practiceScrollIndex}</div>
          <div>signToWrite: {signToWrite}</div>
          <div>writtenSign: {writtenSign}</div>
          <div>userText: {userText}</div>
          <div>
            practiceStatistics:{' '}
            <span>
              correctHits: {practiceStatistics.correctHits}, incorrectHits:{' '}
              {practiceStatistics.incorrectHits}, elapsedTime:{' '}
              {practiceStatistics.elapsedTime},
            </span>
          </div>
          <div>os: {os.name}</div>
          <div>
            keysDown: [
            {keysDown.map((k) => (
              <span key={k}>{k}</span>
            ))}
            ]
          </div>
          <div>
            charsIntroduced: [
            {charsIntroduced.map((c) => (
              <span key={c}>{c}</span>
            ))}
            ]
          </div>
        </div>
      </>
    );
  }

  return (
    <Layout>
      <SEO
        title={intl.formatMessage({
          id: 'site.navigation.statistics',
          defaultMessage: 'Statistics',
        })}
      />
      <section className="statistics__section">
        <div className="container">
          <h2>
            <FormattedMessage
              id="site.navigation.statistics"
              defaultMessage="Statistics"
            />{' '}
            -{' '}
            <FormattedMessage
              id="site.underDevelopment"
              defaultMessage="Under development"
            />
          </h2>
          <div className="statistics">
            <div className="statistics__lessonStat">
              {charsToLearn.map((char, index) => {
                return renderLessonStatisticItem(char, index);
              })}
            </div>
            {renderStateProps()}
          </div>
        </div>
      </section>
      {/* <Keyboard className={'TypewriterBoard__keyboard'} /> */}
    </Layout>
  );
}
