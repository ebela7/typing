import { useIntl, FormattedMessage } from 'gatsby-plugin-intl';
import React from 'react';

import { Layout, SEO, KeyboardKeyStatusses } from '@components';

const ManualPage = () => {
  const intl = useIntl();

  return (
    <Layout className="manual">
      <SEO
        title={intl.formatMessage({
          id: 'site.manual',
          defaultMessage: 'User manual',
        })}
      />
      <section className="manual__section">
        <div className="container">
          <h2 className="manual__title">
            <FormattedMessage id="site.manual" defaultMessage="User manual" />
          </h2>

          <div>
            <p>
              Egyszer eljött az életemben az a pont, amikor úgy döntöttem, hogy
              megtanulok tíz ujjal gépelni. Mivel nagyon unalmas volt könyvből
              tanulni, gondoltam írok egy egyszerű kis segédprogramot. Így
              kezdődött az egész... és ezért attól tartok, nem én vagyok a
              megfelelő személy arra, hogy elmondjam, hogyan kell megtanulni
              helyesen gépírni. Mégis, pár alapgondolatot megosztanék, ami
              bizonyára hasznodra lesz.
            </p>
            <ul>
              <li>Kezdetben ne a sebességre, hanem a pontosságra törekedj!</li>
              <li>Ne siess a leckékkel!</li>
              <li>Ügyelj a helyes test- és kéztartásra!</li>
              <li>Csak a monitort figyeld, ne a billentyűzetet!</li>
              <li>Legyél kitartó!</li>
            </ul>
            <p>
              Ez a program elsősorban azoknak készült akik még nem tudnak
              rendesen tíz ujjal gépelni, de szeretnék megtanulni.
            </p>
            <p>
              Ha még nem tudsz gépírni, de a munkád megkövetelné, akkor az
              alábbi stratégiát javaslom, mivel nekem személy szerint bevált.
            </p>
            <p>
              Először is, határozd el, hogy megtanulod. Tudom, furcsán hangzik,
              de mivel kellemetlen időszaknak nézel elé, fontos, hogy
              elhatározásod szilárd legyen. Fontos, hogy akkor kezdj neki,
              amikor előre láthatóan a következő 3-5 hónapban lesz esélyed időt
              szánni rá. Például, nem a legjobb egy új munkahelyen nekikezdeni -
              noha én pont ezt tettem.
            </p>
            <p>
              Másodszor, égesd fel a hidat magad mögött, ne hagy egérutat
              visszafelé. Ha nem viszed át a gyakorlatba amit tanultál, minden
              hiába.
            </p>
            <p>
              Harmadszor, légy kitartó. Ne szeleburdi, nem kell erőlködni,
              "csak" kitartani.
            </p>
            <p>
              Kicsit részletezve, én magamon azt vettem észre, hogy egyszerűen
              nem tudok egyik napról a másikra átállni a tíz ujjas gépírásra,
              mivel még nem tudok egyáltalán gépelni. Szóval először is el kell
              kezdenem tanulni. Napi fél óra bizonyára megteszi. De mindenképp a
              "gyakran keveset" szabály jobb, mint a "ritkán sokat". A fő, hogy
              a kéz megjegyezze a billentyűk helyzetét. Nincs ebbe semmi
              nehézség, amikor csak egy kis idő adódik, reggel, délben vagy
              este, de legjobb olyan gyakran amikor csak tudsz, gyakorolj. Ez
              eddig egyszerű.
            </p>
            <p>
              Ami viszont nehéz, az az, hogy mi lesz addig a munkahelyen, vagy
              ott ahol éppen írni kell? Nos, semmi. Marad ami van, pötyögni
              ahogy tudsz, mert kell. Ezért minél hamarabb el kell jutni a
              billentyűzet minimális ismeretéig, a teljes ABC-t tudni érdemes
              vakon gépelni.
            </p>
            <p>
              És akkor jöhet a nagy váltás. Felhagyni a pötyögéssel, és átállni
              a tíz ujjal való írásra. Igen, a munkahelyen is. Itt jól jön egy
              kis diplomácia, fel kell tárni a kollégák előtt is, hogy az ember
              vált, annak érdekében, hogy később hatékonyabb legyen, gyorsabban
              tudjon írni. Egy valamire való főnök ezt megérti, sőt támogatja. A
              kollégák meg majd kérdezgetnek, hogy "na, hogy megy" ezzel is
              motiválva, hogy tanulj. De senkit se áltatnék azzal, hogy ez
              egyszerű lesz, mármint az első olyan nap a munkában, amikor
              megpróbálunk helyesen gépelni. Ó nem, az kínos, semmi sem úgy
              sikerül ahogy kellene. De ez nem baj, minden kezdet nehéz, és az
              ember esőre elbukik. De azt javaslom, hogy csak vészhelyzetben
              váltsunk vissza, ezt is csak orvosoknak, tűzoltóknak és
              rendőröknek.
            </p>
            <p>
              Eleinte az ember kínlódik, kifárad, és kudarcosnak érzi a napját.
              Szerintem ez is rendben van. Mert ahogy halad az idő, és kínlódva
              vonszolják magukat a hetek lassan-lassan, egyik a másik után,
              biztos vagyok benne, hogy észre vesszük, hogy valami javulás
              mutatkozik, érezzük, hogy fog ez menni. Erre az időre érdemes egy
              kis pihenőt hagyni az élet egyéb nehézségeiből, kicsit lazítani,
              és arra összpontosítani, hogy "álljunk át". Mentálisan lépjük át a
              küszöböt, az "én nem tudok tíz ujjal gépelni" oldalról a "nem
              nézem a billentyűzetet" oldalra.
            </p>
            <p>
              A szabályok egyszerűek. Tényleg ne nézzük többé a billentyűzetet,
              mert akkor olyanok vagyunk, mint a kényszerbetegek, a függők, nem
              tudunk leállni. Pedig le kell. Ha nem találjuk el a betűt, nem
              baj. Ott a törlés gomb. Próbáljuk meg újra, türelemmel, ha kell,
              lassabban. Sokan hajlamosak bepipulni, és egyre gyorsabban verni a
              billentyűzetet. Én inkább azt javasolnám, hogy ha elkavarodtunk,
              vegyünk egy nagy lélegzetet, és gondoljuk újra, mit is fogunk
              csinálni. Melyik ujjal, hova? Tanultuk már, csak elő kell hívni.
              Mint minden tanulási folyamatnál, itt is ez a legfontosabb, a
              helyes rögzítés. Minden trükk, amivel megkerüljük a munkát
              ellenünk hat, így tényleg nem érdemes csalni.
            </p>
            <p>
              Ha szépen, kitartóan dolgozunk, azt fogjuk észlelni, hogy megy ez
              nekünk. Kérlek, ne kérdezzétek, mennyi idő múlva, mindenki más,
              kinek gyorsabban, kinek lassabban megy. és ha már megy, akkor ne
              bízzuk el magunkat. Térjünk vissza időről időre gyakorolni, mert
              "gyakorlat teszi a mestert". Ekkor a gyakorlatokat akár örömmel is
              lehet csinálni, hiszen semmi sem sürgeti az embert, és már látható
              a hozadéka, hogy megéri.
            </p>

            <h3>Egészségvédelem</h3>

            <p>
              Szeretném felhívni a figyelmedet, hogy a gépírás nem veszélytelen
              dolog. Nekem igen komoly ínhüvely-gyulladásom alakult ki egy
              időben, a jobb kezem kis ujján. Ez a magyar ékezetes billentyűk
              miatt van. Dióhéjban csak annyit, hogy a billentyűzet a gépírókból
              fejlődött ki, az pedig az angol karakterekre épült. Amikor a
              magyar (és sok más) billentyűzetet sztenderdizálták, az adott
              nyelvre jellemző eltérések jellemzően a jobb oldali, "kieső",
              eredetileg nem alfanumerikus részen kaptak helyet. Így állt elő az
              a szörnyű helyzet, hogy noha az ember hüvelykujja kifordítható, és
              igen ügyes - lásd mobiltelefonon a teljes ABC-t ez az ujj gépeli -
              addig a billentyűzeten mindössze 1 billentyű lenyomásáért felel, a
              suta kisujj pedig legalább 13 billentyűért, ráadásul teljesen
              természetellenes kiszögellésben.
            </p>
            <p>
              Tudomásom van továbbá az ujjhegyek és a csukló idegeinek "írógép"
              okozta betegségeiről. Ezek komoly témák, így nem kontárkodnék
              bele, hanem javaslom, hogy kérdezd meg orvosodat, vagy ha van,
              munkavédelmi szakemberedet, vagy még jobb, nézz után az interneten
              a helyes test és kéztartásnak, illetve kockázatoknak. Amennyiben
              kezedben szokatlan, kellemetlen érzések jönnek elő, konzultálj
              orvosoddal.
            </p>

            <h3>Ajánlott itodalom</h3>
            <ul>
              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/4_1586_002_101030.pdf"
                  target="_blank"
                >
                  Hartyániné Barta Judit: A tízujjas vakírás elsajátítása I.
                  Helyes test- és kéztartás, billentyűzetkezelés. Az alapsor
                  betűi, szóköz, sorváltás (enter)
                </a>
              </li>

              <li>
                <a href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/4_1586_007_101030.pdf">
                  Hartyániné Barta Judit: Az irodai gépírás alapjai: I. A
                  másolás technikájának fejlesztése
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/4_1586_008_101030.pdf"
                  target="_blank"
                >
                  Hartyániné Barta Judit: Az irodai gépírás alapjai: II. Az
                  írássebesség fokozása
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_001_101015.pdf"
                  target="_blank"
                >
                  Elbert Gyuláné: Az elektronikus írás előkészítése - a tízujjas
                  vakírás alapjai
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_008_101030.pdf"
                  target="_blank"
                >
                  Papp Eszter: Írás diktálás, hallás után
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_003_101030.pdf"
                  target="_blank"
                >
                  Papp Eszter: Billentyűkezelés II. Az e, z, o, r, ő, n, y, ó, v
                  betűk
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_004_101030.pdf"
                  target="_blank"
                >
                  Papp Eszter: Billentyűkezelés III. A p, u, b, ö, h, í, kötőjel
                  és a c betűk betűk
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_005_101030.pdf"
                  target="_blank"
                >
                  Papp Eszter: Billentyűkezelés IV. Az ú, x, ü, w, ű, q betűk
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_007_101030.pdf"
                  target="_blank"
                >
                  Jakabné dr.Zubály Anna: A másolás technikája
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/4_1586_009_101030.pdf"
                  target="_blank"
                >
                  Jakabné dr.Zubály Anna: Az irodai gépírás alapjai III. A
                  hallás utáni írás technikája
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_009_101030.pdf"
                  target="_blank"
                >
                  Jakabné dr. Zubály Anna: Írás könnyen, gyorsan, hibátlanul
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_tartalomelem_002_munkaanyag_091231.pdf"
                  target="_blank"
                >
                  Jakabné dr. Zubály Anna: Billentyűkezelés I. Az I, T, M, Á, G
                  betűk A vessző, a pont és a váltó kezelése
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_tartalomelem_002_munkaanyag_091231.pdf"
                  target="_blank"
                >
                  Jakabné dr. Zubály Anna: Billentyűkezelés I. Az I, T, M, Á, G
                  betűk A vessző, a pont és a váltó kezelése
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_tartalomelem_006_munkaanyag_100531.pdf"
                  target="_blank"
                >
                  Jakabné dr. Zubály Anna: Billentyűkezelés V. Számok, írásjelek
                  írása
                </a>
              </li>

              <li>
                <a
                  href="https://www.nive.hu/Downloads/Szakkepzesi_dokumentumok/Bemeneti_kompetenciak_meresi_ertekelesi_eszkozrendszerenek_kialakitasa/16_1617_tartalomelem_011_munkaanyag_091231.pdf"
                  target="_blank"
                >
                  Jakabné dr. Zubály Anna: Cél: a hibátlan írás!
                </a>
              </li>

              <li>
                <a
                  href="http://szit.hu/download/oktatas/gepeles/gepiras_oktato_anyag_v5.0.pdf"
                  target="_blank"
                >
                  Sallai András: Gépírás
                </a>
              </li>
            </ul>
          </div>

          <KeyboardKeyStatusses />
        </div>
      </section>
    </Layout>
  );
};

export default ManualPage;
