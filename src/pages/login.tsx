import { FormattedMessage, useIntl } from 'gatsby-plugin-intl';
import React, { useState, useEffect } from 'react';

import { Button } from '@components';
import { Layout, SEO, FirebaseUIAuth } from '@components';
import useFirebase from '@utils/useFirebase';

const LoginPage = () => {
  const intl = useIntl();
  const [isSignedIn, setIsSignedIn] = useState(false);

  const firebase = useFirebase();

  useEffect(() => {
    if (!firebase) return;

    // @ts-ignore
    return firebase.auth().onAuthStateChanged((user: any) => {
      setIsSignedIn(!!user);
    });
  }, [firebase]);

  function getContent() {
    if (firebase) {
      if (isSignedIn) {
        return (
          <div className="login__form">
            <Button
              onClick={
                // @ts-ignore
                () => firebase.auth().signOut()
              }
            >
              <FormattedMessage id="site.logout" defaultMessage="Sign-out" />
            </Button>
          </div>
        );
      } else {
        return <FirebaseUIAuth />;
      }
    }
  }

  return (
    <Layout className="login">
      <SEO
        title={intl.formatMessage({
          id: 'site.login',
          defaultMessage: 'Login',
        })}
      />
      <section className="loginForm__section">
        <div className="container">
          <h2 className="login__title">
            <FormattedMessage id="site.login" defaultMessage="Login" />
          </h2>
          <div>{getContent()}</div>
        </div>
      </section>
    </Layout>
  );
};

export default LoginPage;
