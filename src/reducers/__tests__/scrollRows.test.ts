import { scrollRows } from '@actions/scrollRows';

import scrollRowsReducer, { ScrollState } from '../scrollRows';

const defaultState: ScrollState = {
  practiceScrollIndex: 0,
};

describe('scrollRowsReducer', () => {
  it('returns the specified row', () => {
    const newState = scrollRowsReducer(defaultState, scrollRows(0));

    expect(newState.practiceScrollIndex).toEqual(0);

    // change state again
    const secondState = scrollRowsReducer(newState, scrollRows(1));
    const { practiceScrollIndex } = secondState;

    expect(practiceScrollIndex).toEqual(1);
  });
});
