import { getUserInputFocus, setUserInputFocus } from '@actions/userInputFocus';

import userInputFocusReducer, { FocusUserInputState } from '../userInputFocus';

const defaultState: FocusUserInputState = {
  isUserInputFocused: false,
  shouldUserInputFocus: false,
};

describe('userInputFocusReducer', () => {
  it('gets the proper boolean whenever the user input is focused or not', () => {
    const newState = userInputFocusReducer(
      defaultState,
      getUserInputFocus(true)
    );

    expect(newState).toStrictEqual({
      isUserInputFocused: true,
      shouldUserInputFocus: false,
    });

    // change state again
    const secondState = userInputFocusReducer(
      defaultState,
      getUserInputFocus(false)
    );

    expect(secondState).toStrictEqual({
      isUserInputFocused: false,
      shouldUserInputFocus: false,
    });
  });

  it('sets the the user input focused or blurred based on boolean value', () => {
    const newState = userInputFocusReducer(
      defaultState,
      setUserInputFocus(true)
    );

    expect(newState).toStrictEqual({
      isUserInputFocused: false,
      shouldUserInputFocus: true,
    });

    // change state again
    const secondState = userInputFocusReducer(
      defaultState,
      setUserInputFocus(false)
    );

    expect(secondState).toStrictEqual({
      isUserInputFocused: false,
      shouldUserInputFocus: false,
    });
  });
});
