import { userIsTouching } from '@actions/userIsTouching';

import userIsTouchingReducer, { UserIsTouchingState } from '../userIsTouching';

const defaultState: UserIsTouchingState = {
  isTouchDevice: false,
};

describe('userIsTouchingReducer', () => {
  it('returns whenever device is touch devide or not', () => {
    const newState = userIsTouchingReducer(defaultState, userIsTouching(true));

    expect(newState.isTouchDevice).toEqual(true);

    // change state again
    const secondState = userIsTouchingReducer(newState, userIsTouching(false));
    const { isTouchDevice } = secondState;

    expect(isTouchDevice).toEqual(false);
  });
});
