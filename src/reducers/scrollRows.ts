import { ScrollRowsAction } from '@actions';
export type ScrollState = {
  practiceScrollIndex: number;
};

const initialState: ScrollState = {
  practiceScrollIndex: 0,
};

export default function scrollRowsReducer(
  state: ScrollState = initialState,
  action: ScrollRowsAction
) {
  const { rowIndex, type } = action;
  if (type === 'SCROLL_ROWS') {
    return {
      ...state,
      practiceScrollIndex: rowIndex,
    };
  }
  return state;
}
