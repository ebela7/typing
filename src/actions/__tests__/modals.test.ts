import {
  discoveryModalClosed,
  introductionModalClosed,
  summaryModalClosed,
} from '@actions/modals';

describe('modal actions', () => {
  it('should create an action when discovery modal is closed', () => {
    const expectedAction = {
      type: 'DISCOVERY_MODAL_CLOSED',
    };

    expect(discoveryModalClosed()).toEqual(expectedAction);
  });
  it('should create an action when introduction modal is closed', () => {
    const expectedAction = {
      type: 'INTRODUCTION_MODAL_CLOSED',
    };

    expect(introductionModalClosed()).toEqual(expectedAction);
  });
  it('should create an action when summary modal is closed', () => {
    const expectedAction = {
      type: 'SUMMARY_MODAL_CLOSED',
      props: {},
    };

    expect(summaryModalClosed({})).toEqual(expectedAction);

    const expectedRepeatAction = {
      type: 'SUMMARY_MODAL_CLOSED',
      props: { repeat: true },
    };
    expect(summaryModalClosed({ repeat: true })).toEqual(expectedRepeatAction);
  });
});
