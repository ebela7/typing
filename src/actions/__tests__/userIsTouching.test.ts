import { userIsTouching } from '@actions/userIsTouching';

describe('userIsTouching actions', () => {
  it('should create an action to determinate if the device has touch capabilities', () => {
    const expectedAction = {
      type: 'USER_IS_TOUCHING',
      isTouchDevice: true,
    };

    expect(userIsTouching(true)).toEqual(expectedAction);
  });
});
