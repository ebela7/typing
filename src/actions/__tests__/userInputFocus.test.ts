import { getUserInputFocus, setUserInputFocus } from '@actions/userInputFocus';

describe('userInputFocus actions', () => {
  it('should create an action to inform components about the user input focus state', () => {
    const expectedAction = {
      type: 'GET_USER_INPUT_FOCUS',
      focus: true,
    };

    expect(getUserInputFocus(true)).toEqual(expectedAction);
  });

  it('should create an action to focus or blur the user input', () => {
    const expectedAction = {
      type: 'SET_USER_INPUT_FOCUS',
      focus: true,
    };

    expect(setUserInputFocus(true)).toEqual(expectedAction);
  });
});
