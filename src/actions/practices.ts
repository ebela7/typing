export type PracticeAction = ReturnType<
  typeof startPractice | typeof pausePractice | typeof continuePractice
>;

// START_PRACTICE

export const START_PRACTICE = 'START_PRACTICE';

export function startPractice() {
  return { type: START_PRACTICE };
}

// PAUSE_PRACTICE

export const PAUSE_PRACTICE = 'PAUSE_PRACTICE';

export function pausePractice() {
  return { type: PAUSE_PRACTICE };
}

// CONTINUE_PRACTICE

export const CONTINUE_PRACTICE = 'CONTINUE_PRACTICE';

export function continuePractice() {
  return { type: CONTINUE_PRACTICE };
}
