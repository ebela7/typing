// DISCOVERY_MODAL_CLOSED

export type ModalClosedAction = ReturnType<
  | typeof discoveryModalClosed
  | typeof introductionModalClosed
  | typeof summaryModalClosed
>;

export const DISCOVERY_MODAL_CLOSED = 'DISCOVERY_MODAL_CLOSED';

export function discoveryModalClosed() {
  return { type: DISCOVERY_MODAL_CLOSED };
}

// INTRODUCTION_MODAL_CLOSED

export const INTRODUCTION_MODAL_CLOSED = 'INTRODUCTION_MODAL_CLOSED';

export function introductionModalClosed() {
  return { type: INTRODUCTION_MODAL_CLOSED };
}

// SUMMARY_MODAL_CLOSED

export const SUMMARY_MODAL_CLOSED = 'SUMMARY_MODAL_CLOSED';

export function summaryModalClosed(props: { repeat?: boolean }) {
  return { type: SUMMARY_MODAL_CLOSED, props };
}
