// see: https://invertase.io/blog/firebase-with-gatsby

// also: https://firebase.google.com/docs/web/setup#config-object

import firebase from 'firebase/app';
import 'firebase/analytics';
import 'firebase/database';
import 'firebase/firestore';

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import 'firebase/analytics';

// Firebase web config
// https://firebase.google.com/docs/projects/api-keys
/*
const config = {
  apiKey: 'AIzaSyC-qiJJ9Ieefga2yLO4N85YWGROAvp1tnY',
  authDomain: 'manonet.org',
  databaseURL: 'https://manonet-7f714.firebaseio.com',
  projectId: 'manonet-7f714',
  storageBucket: 'manonet-7f714.appspot.com',
  messagingSenderId: '146446290915',
  appId: '1:146446290915:web:64ebb044905d5e98d11ad6',
  measurementId: 'G-J0XT56TPFZ',
};
*/

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const config = {
  apiKey: 'AIzaSyCXOfGPPmR8N1JQ0J_q4Z0kjcBcIVhdsW4',
  authDomain: 'manonet-71d56.firebaseapp.com',
  projectId: 'manonet-71d56',
  storageBucket: 'manonet-71d56.appspot.com',
  messagingSenderId: '255646828046',
  appId: '1:255646828046:web:569c988045e238ba0e1e5f',
  measurementId: 'G-QJ1C52DDPG',
};

let instance: any = null;

export default function getFirebase() {
  if (typeof window !== 'undefined') {
    if (instance) return instance;
    instance = firebase.initializeApp(config);

    firebase.analytics();
    firebase.database();
    firebase.firestore();

    return instance;
  }

  return null;
}
