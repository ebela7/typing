// see: https://github.com/greg-schrammel/react-firebaseui-localized/blob/master/useScript.js

import { useState, useEffect, useRef } from 'react';

function useScript(src: string) {
  const [state, setState] = useState({ loaded: false, error: false });
  const script = document.createElement('script');

  useEffect(() => {
    script.src = src;
    script.async = true;

    const onScriptLoad = () => setState({ loaded: !!src, error: false });

    const onScriptError = (e: Event) => {
      script.remove();
      setState({ loaded: false, error: !!e });
    };

    script.addEventListener('load', onScriptLoad);
    script.addEventListener('error', onScriptError);

    document.body.appendChild(script);
    return () => {
      script.removeEventListener('load', onScriptLoad);
      script.removeEventListener('error', onScriptError);
    };
  }, [src]);

  return [state.loaded, state.error];
}

export default useScript;
