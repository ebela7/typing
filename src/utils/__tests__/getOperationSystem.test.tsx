import { getOperationSystem } from '../getOperationSystem';

const setOperationSystem = function (userAgentString: string) {
  Object.defineProperty(global.navigator, 'userAgent', {
    value: userAgentString,
    writable: true,
  });

  return getOperationSystem();
};

describe('getOperationSystem', () => {
  // afterEach(() => {
  //   // default user agent in test environment
  //   setOperationSystem(
  //     'Mozilla/5.0 (linux) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.4.0'
  //   );
  // });

  it('returns unknown as default', () => {
    const os = setOperationSystem(
      'Mozilla/5.0 (PlayStation 4 1.70) AppleWebKit/536.26 (KHTML, like Gecko)'
    );

    expect(os).toEqual({
      name: 'Unknown',
      os: 'unknown',
      sign: '&#xf17a;',
    });
  });

  it('returns Windows', () => {
    const os = setOperationSystem(
      'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
    );

    expect(os).toEqual({
      name: 'Windows',
      os: 'windows',
      sign: '&#xf17a;',
    });
  });

  it('returns Linux', () => {
    const os = setOperationSystem(
      'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
    );

    expect(os).toEqual({
      name: 'Linux',
      os: 'osx',
      sign: '&#xf17c;',
    });
  });

  it('returns iOS', () => {
    const os = setOperationSystem(
      'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1'
    );

    expect(os).toEqual({
      name: 'iOS',
      os: 'osx',
      sign: '&#xf179;',
    });
  });

  it('returns MacOS', () => {
    const os = setOperationSystem(
      'Macintosh; Intel Mac OS X 11_0_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
    );

    expect(os).toEqual({
      name: 'MacOS',
      os: 'osx',
      sign: '⌘',
    });
  });

  it('returns Android', () => {
    const os = setOperationSystem(
      'Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36'
    );

    expect(os).toEqual({
      name: 'Android',
      os: 'android',
      sign: '&#xf17b;',
    });
  });
});
