import { FormattedMessage } from 'gatsby-plugin-intl';
import React from 'react';
import { useDispatch } from 'react-redux';

import { resetState } from '@actions';
import { Button } from '@components';

import './ResetInfoBox.scss';

export default function ResetInfoBox() {
  const dispatch = useDispatch();
  return (
    <div className="ResetInfoBox">
      <div className="ResetInfoBox__desc">
        <FormattedMessage
          id="typing.reset.desc"
          defaultMessage="If you think, that something went wrong with the app, you can reset your data to the initial state."
        />
      </div>

      <div className="ResetInfoBox__alert">
        <div>
          <FormattedMessage id="warning.title" defaultMessage="Warning!" />
        </div>
        <div>
          <FormattedMessage
            id="typing.reset.warning"
            defaultMessage="This action deletes all typing related data."
          />
        </div>
      </div>

      <Button
        onClick={() => {
          dispatch(resetState());
          location.reload();
        }}
        color="primary"
      >
        {' '}
        <FormattedMessage id="typing.reset" defaultMessage="Reset" />
      </Button>
    </div>
  );
}
