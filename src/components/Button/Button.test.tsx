import { mount, shallow } from 'enzyme';
import React from 'react';

import Button, { BUTTON_CLASS_NAME } from './Button';

describe('<Button>', () => {
  it('renders a Button properly', () => {
    const wrapper = mount(<Button>Hello</Button>);

    expect(BUTTON_CLASS_NAME).toBe('Button');
    expect(wrapper.find(`.${BUTTON_CLASS_NAME}`)).toHaveLength(1);
  });

  it('has default onClick function', () => {
    spyOn(console, 'error');
    const button = shallow(<Button>Click me!</Button>);
    button
      .find(`.${BUTTON_CLASS_NAME}`)
      .simulate('click', { preventDefault: () => {} });
    expect(console.error).toHaveBeenCalledTimes(0);
  });

  it('has default onKeyDown function', () => {
    spyOn(console, 'error');
    const button = shallow(<Button>Click me!</Button>);
    const options = {
      preventDefault: () => {},
      key: 'Enter',
    };
    button.find(`.${BUTTON_CLASS_NAME}`).simulate('keyDown', options);
    expect(console.error).toHaveBeenCalledTimes(0);
  });

  it('executes the onClick handler when a non-disabled button is clicked', () => {
    const mockOnClick = jest.fn();
    const button = shallow(<Button onClick={mockOnClick}>Click me!</Button>);
    button
      .find(`.${BUTTON_CLASS_NAME}`)
      .simulate('click', { preventDefault: () => {} });
    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });

  it('executes the onKeyDown handler when a non-disabled button is clicked', () => {
    const mockOnKeyDown = jest.fn();
    const button = shallow(
      <Button onKeyDown={mockOnKeyDown}>Click me!</Button>
    );
    const options = {
      preventDefault: () => {},
      key: 'Enter',
    };
    button.find(`.${BUTTON_CLASS_NAME}`).simulate('keyDown', options);
    expect(mockOnKeyDown).toHaveBeenCalledTimes(1);
  });

  it('does not execute the onClick handler when a disabled button is clicked', () => {
    const mockOnClick = jest.fn();
    const button = shallow(
      <Button disabled onClick={mockOnClick}>
        Click me!
      </Button>
    );
    button
      .find(`.${BUTTON_CLASS_NAME}`)
      .simulate('click', { preventDefault: () => {} });
    expect(mockOnClick).not.toHaveBeenCalled();
  });

  it('does not execute the onKeyDown handler when a disabled button is clicked', () => {
    const mockOnKeyDown = jest.fn();
    const button = shallow(
      <Button disabled onKeyDown={mockOnKeyDown}>
        Click me!
      </Button>
    );
    const options = {
      preventDefault: () => {},
      key: 'Enter',
    };
    button.find(`.${BUTTON_CLASS_NAME}`).simulate('keyDown', options);
    expect(mockOnKeyDown).not.toHaveBeenCalled();
  });

  it('accept additional classes', () => {
    const button = shallow(<Button className="myFancyClass">Button</Button>);
    expect(button.find('.myFancyClass')).toHaveLength(1);
  });

  it('accept displayType property', () => {
    const button = shallow(<Button type="default">Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}--default`)).toHaveLength(1);
  });

  it('accept size property', () => {
    const button = shallow(<Button size="small">Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}--small`)).toHaveLength(1);
  });

  it('accept color property', () => {
    const button = shallow(<Button color="danger">Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}--danger`)).toHaveLength(1);
  });

  it('displays an full width button when the flag block is passed', () => {
    const button = shallow(<Button block>Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}--block`)).toHaveLength(1);
  });

  it('displays a square button when the flag square is passed', () => {
    const button = shallow(<Button square>Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}--square`)).toHaveLength(1);
  });

  it('displays a disabled button when the flag disabled is passed', () => {
    const button = shallow(<Button disabled>Disabled Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}`).is('[disabled]')).toBe(true);
  });

  it('accept role property', () => {
    const button = shallow(<Button role="menuitem">Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}`).prop('role')).toBe('menuitem');
  });

  it('accept tabIndex property', () => {
    const button = shallow(<Button tabIndex={-2}>Button</Button>);
    expect(button.find(`.${BUTTON_CLASS_NAME}`).prop('tabIndex')).toBe(-2);
  });
});
