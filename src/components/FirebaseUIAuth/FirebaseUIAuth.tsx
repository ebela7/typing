// see: https://github.com/greg-schrammel/react-firebaseui-localized/blob/master/index.js

import firebase from 'firebase';
import { useIntl } from 'gatsby-plugin-intl';
import React, { useEffect, useRef } from 'react';

import { RtlLangs } from '@intl/languages';
import {
  ROUTE_PATH_TYPEWRITER,
  ROUTE_PATH_PRIVACY_POLICY,
  ROUTE_PATH_TERMS_OF_USE,
} from '@routes';
import useFirebase from '@utils/useFirebase';
import useScript from '@utils/useScript';

function FirebaseUIAuth() {
  const intl = useIntl();
  const lang = intl.locale;
  const firebaseApp = useFirebase();
  const firebaseuiRef = useRef(null);

  const firebaseVersion = '4.7.1';

  // Configure FirebaseUI.
  const config = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    ],
    // callbacks: {
    //   // Avoid redirects after sign-in.
    //   signInSuccessWithAuthResult: () => false,
    // },
    signInSuccessUrl: ROUTE_PATH_TYPEWRITER,
    tosUrl: ROUTE_PATH_TERMS_OF_USE,
    privacyPolicyUrl: ROUTE_PATH_PRIVACY_POLICY,
  };

  // See: https://github.com/firebase/firebaseui-web/blob/master/LANGUAGES.md
  const [loaded, error] = useScript(
    `https://www.gstatic.com/firebasejs/ui/${firebaseVersion}/firebase-ui-auth__${lang.replace(
      '-',
      '_'
    )}.js`
  );

  useEffect(() => {
    // @ts-ignore
    window.firebase = firebase;
  }, []);

  useEffect(() => {
    if (!firebaseApp) return;

    // @ts-ignore
    const auth = firebaseApp.auth();
    // @ts-ignore
    let firebaseUI = null;
    if (!loaded) return;
    if (error) throw error;
    firebaseUI =
      // @ts-ignore
      window.firebaseui.auth.AuthUI.getInstance() ||
      // @ts-ignore
      new window.firebaseui.auth.AuthUI(auth);
    firebaseuiRef.current && firebaseUI.start(firebaseuiRef.current, config);

    return () => {
      // @ts-ignore
      firebaseUI && firebaseUI.delete();
    };
  }, [firebaseApp, error, loaded]);

  return (
    <>
      <link
        type="text/css"
        rel="stylesheet"
        href={`https://www.gstatic.com/firebasejs/ui/${firebaseVersion}/firebase-ui-auth${
          RtlLangs.includes(lang) ? '-rtl' : ''
        }.css`}
      />
      <div ref={firebaseuiRef} />
    </>
  );
}

export default FirebaseUIAuth;
