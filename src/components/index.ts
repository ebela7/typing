import Button from './Button';
import CookieConsent from './CookieConsent';
import FirebaseUIAuth from './FirebaseUIAuth';
import Hand from './Hand';
import Keyboard from './Keyboard';
import KeyboardKey from './KeyboardKey';
import KeyboardKeyStatusses from './KeyboardKeyStatusses';
import LanguageSwitcher from './LanguageSwitcher';
import { Layout, Header, Footer, SEO } from './Layout';
import {
  PracticeIntroductionModal,
  PracticeSummaryModal,
  ExploreMoreModal,
} from './PracticeModals';
import PracticeText, {
  PracticeTextChar,
  RenderPracticeRows,
  PracticeProgressBar,
} from './PracticeText';
import { ResetInfoBox } from './ResetInfoBox';

export {
  Button,
  CookieConsent,
  FirebaseUIAuth,
  Hand,
  Keyboard,
  KeyboardKey,
  KeyboardKeyStatusses,
  LanguageSwitcher,
  PracticeProgressBar,
  Layout,
  Header,
  Footer,
  SEO,
  PracticeIntroductionModal,
  PracticeSummaryModal,
  ExploreMoreModal,
  PracticeText,
  RenderPracticeRows,
  PracticeTextChar,
  ResetInfoBox,
};

export * from './PracticeModals';
export * from './User';
