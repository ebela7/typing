import React from 'react';

import { mountWithIntl } from '@utils/intlEnzymeTestHelper';

import KeyboardKeyStatusses from './KeyboardKeyStatusses';

describe('<KeyboardKeyStatusses>', () => {
  it('renders KeyboardKeyStatusses properly', () => {
    const wrapper = mountWithIntl(<KeyboardKeyStatusses />);

    expect(wrapper.find('.statuses')).toHaveLength(1);
    expect(wrapper.find('.statuses__list')).toHaveLength(1);
    expect(wrapper.find('.statuses__item')).toHaveLength(14);

    const item = wrapper.find('.statuses__item').first();

    expect(item.find('.statuses__key')).toHaveLength(1);
    expect(item.find('.statuses__text')).toHaveLength(1);
    expect(item.find('.statuses__title')).toHaveLength(1);
    expect(item.find('.statuses__desc')).toHaveLength(1);
  });
});
