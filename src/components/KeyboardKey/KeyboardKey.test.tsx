import { mount } from 'enzyme';
import React from 'react';

import KeyboardKey from './KeyboardKey';

function renderKey(props = {}) {
  return (
    <svg
      className="statuses__keysvg"
      version="1.1"
      viewBox={`0 0 100 100`}
      textAnchor="middle"
    >
      <KeyboardKey
        x={20}
        y={30}
        width={40}
        height={50}
        displayedLevel="to"
        layout="102/105-ISO"
        iso="C01"
        code="KeyA"
        hand="left"
        finger="little"
        {...props}
      />
    </svg>
  );
}

describe('<KeyboardKey>', () => {
  it('renders properly', () => {
    const wrapper = mount(renderKey());

    expect(wrapper.find('.key')).toHaveLength(1);
    expect(wrapper.find('.key__shadow')).toHaveLength(1);
    expect(wrapper.find('.key__keyBg')).toHaveLength(1);
    expect(wrapper.find('.key__label')).toHaveLength(1);
    expect(wrapper.find('.key__bump')).toHaveLength(0);
    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--uncovered'
    );
    expect(wrapper.find('.key__label').text()).toBe('?');
  });

  it('renders missed state properly', () => {
    const wrapper = mount(
      renderKey({
        succeedState: 'missed',
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--missed key--uncovered'
    );
  });

  it('renders correct state properly', () => {
    const wrapper = mount(
      renderKey({
        succeedState: 'correct',
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--correct key--uncovered'
    );
  });

  it('renders error state properly', () => {
    const wrapper = mount(
      renderKey({
        succeedState: 'error',
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--error key--uncovered'
    );
  });

  it('renders toPressFirst state properly', () => {
    const wrapper = mount(
      renderKey({
        marker: 'toPressFirst',
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--toPressFirst key--uncovered'
    );
  });

  it('renders toPressSecond state properly', () => {
    const wrapper = mount(
      renderKey({
        marker: 'toPressSecond',
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--toPressSecond key--uncovered'
    );
  });

  it('renders toLearn state properly', () => {
    const wrapper = mount(
      renderKey({
        keyTops: {
          to: {
            toLearn: true,
          },
        },
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--toLearn key--uncovered'
    );
  });

  it('renders learned state properly', () => {
    const wrapper = mount(
      renderKey({
        keyTops: {
          to: {
            learned: true,
          },
        },
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--learned key--uncovered'
    );
  });

  it('renders uncovered state properly', () => {
    const wrapper = mount(renderKey());

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--uncovered'
    );
  });

  it('renders pressed state properly', () => {
    const wrapper = mount(renderKey({ pressure: 'pressed' }));

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--uncovered key--pressed'
    );
  });

  it('renders locked state properly', () => {
    const wrapper = mount(renderKey({ pressure: 'locked' }));

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--uncovered key--locked'
    );
  });

  it('renders explored state properly', () => {
    const wrapper = mount(
      renderKey({
        keyTops: {
          to: {
            label: 'a',
          },
        },
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe('key C01 KeyA');
    expect(wrapper.find('.key__label').text()).toBe('a');
  });

  it('renders dead key properly', () => {
    const wrapper = mount(
      renderKey({
        keyTops: {
          to: {
            label: 'a',
            dead: true,
          },
        },
      })
    );

    expect(wrapper.find('.key').prop('className')).toBe(
      'key C01 KeyA key--dead'
    );
  });

  it('renders bump on F key', () => {
    const KeyF = mount(renderKey({ iso: 'C04' }));

    const x = KeyF.find('.key__bump').prop('x');
    const y = KeyF.find('.key__bump').prop('y');
    expect(Number(x).toFixed(2)).toBe('516.67');
    expect(Number(y).toFixed(2)).toBe('279.33');
  });

  it('renders bump on J key', () => {
    const KeyJ = mount(renderKey({ iso: 'C07' }));

    const x = KeyJ.find('.key__bump').prop('x');
    const y = KeyJ.find('.key__bump').prop('y');
    expect(Number(x).toFixed(2)).toBe('816.67');
    expect(Number(y).toFixed(2)).toBe('279.33');
  });

  it('renders Enter key', () => {
    const Enter = mount(
      renderKey({ iso: 'C13', enterPath: 'some path string' })
    );

    expect(Enter.find('path')).toHaveLength(2);
    expect(Enter.find('rect')).toHaveLength(0);
    expect(Enter.find('.key__keyBg').prop('d')).toBe('some path string');
  });

  it('renders Enter key for 101/104-ANSI', () => {
    const Enter = mount(
      renderKey({
        iso: 'C13',
        enterPath: 'some path string',
        layout: '101/104-ANSI',
      })
    );

    expect(Enter.find('path')).toHaveLength(0);
    expect(Enter.find('rect')).toHaveLength(2);
  });

  it('renders Enter key for 101/104-Variant', () => {
    const Enter = mount(
      renderKey({
        iso: 'C13',
        enterPath: 'some path string',
        layout: '101/104-Variant',
      })
    );

    expect(Enter.find('path')).toHaveLength(2);
    expect(Enter.find('rect')).toHaveLength(0);
    expect(Enter.find('.key__keyBg').prop('d')).toBe('some path string');
  });
});
