import getEnterPath from './getEnterPath';

describe('getEnterPath', () => {
  it('returns SVG path for the layout 101/104-Variant', () => {
    const enterPath = getEnterPath({
      layout: '101/104-Variant',
    });

    expect(enterPath).toEqual(
      'M1360 120        A 10 10, 0, 0, 1, 1370 110        L 1480 110        A 10 10, 0, 0, 1, 1490 120        L 1490 280        A 10 10, 0, 0, 1, 1480 290        L 1300 290        A 10 10, 0, 0, 1, 1290 280        L 1290 220        A 10 10, 0, 0, 1, 1300 210        L 1350 210        A 10 10, 0, 0, 0, 1360 200        L 1360 120 Z'
    );
  });

  it('returns SVG path for the layout 103/106-KS', () => {
    const enterPath = getEnterPath({
      layout: '103/106-KS',
    });

    expect(enterPath).toEqual(
      'M1360 120        A 10 10, 0, 0, 1, 1370 110        L 1480 110        A 10 10, 0, 0, 1, 1490 120        L 1490 280        A 10 10, 0, 0, 1, 1480 290        L 1300 290        A 10 10, 0, 0, 1, 1290 280        L 1290 220        A 10 10, 0, 0, 1, 1300 210        L 1350 210        A 10 10, 0, 0, 0, 1360 200        L 1360 120 Z'
    );
  });

  it('returns SVG path for the rest layouts', () => {
    const enterPath = getEnterPath({
      layout: '104/107-ABNT',
    });

    expect(enterPath).toEqual(
      'M1360 120      A 10 10, 0, 0, 1, 1370 110      L 1480 110      A 10 10, 0, 0, 1, 1490 120      L 1490 280      A 10 10, 0, 0, 1, 1480 290      L 1400 290      A 10 10, 0, 0, 1, 1390 280      L 1390 200      A 10 10, 0, 0, 0, 1380 190      L 1370 190      A 10 10, 0, 0, 1, 1360 180      L 1360 120 Z'
    );
  });

  it('returns does not return anything for the 101/104-ANSI layout', () => {
    const enterPath = getEnterPath({
      layout: '101/104-ANSI',
    });

    expect(enterPath).toEqual('');
  });
});
